//Game Pattern
var gamePattern = [];

//User Clicked Pattern
var userClickedPattern = [];

//Array Colors
var buttonColors = ["red", "blue", "green", "yellow"];

//Start the Game
var started = false;

//Game level
var level = 0;

//Keyboard detect start game
$(document).keypress(function() {
  if (!started) {
    $("#level-title").text("Level " + level);
    nextSequence();
    started = true;
  }
});

//Detect buttons Cliked
$(".btn").click(function(){
  //Store the id button clicked
  var userChosenColor = $(this).attr("id");

  //Add contents
  userClickedPattern.push(userChosenColor);

  //Play Sound
  playSound(userChosenColor);

  //Animate Button
  animatePress(userChosenColor);

  //Check Answer
  checkAnswer(userClickedPattern.length - 1);
});

//Next Sequence Function
function nextSequence(){

  //Empty Array Next level
  userClickedPattern = [];

  //Increase Level
  level++;
  $("#level-title").text("Level " + level);

  //Creating a random number
  var randomNumber = Math.floor(Math.random() * 4);

  //Creating Variable Chosen Color
  var randomChosenColor = buttonColors[randomNumber];

  //Add randomChosenColor generated
  gamePattern.push(randomChosenColor);

  //Select button to animate buttonColors
  $("#" + randomChosenColor).fadeIn(100).fadeOut(100).fadeIn(100);

  //Playing the sound selected buttons
  playSound(randomChosenColor);

}

//PlaySound Function
function playSound(randomChosenColor){
  var audio = new Audio("sounds/" + randomChosenColor + ".mp3");
  audio.play();
}

//Animate Press Function
function animatePress(currentColor){
  //Add class to pressed button
  $("#" + currentColor).addClass("pressed ");

  //Remove pressed class
  setTimeout(function(){
    $("#" + currentColor).removeClass("pressed");
  }, 100);
}

//Check Answer Function
function checkAnswer(currentLevel){
  if(gamePattern[currentLevel] === userClickedPattern[currentLevel]){

    console.log("Success");

    if(userClickedPattern.length === gamePattern.length){

      setTimeout(function(){
        nextSequence();
      }, 1000);

    }
  } else {

    playSound("wrong");

    $("body").addClass("game-over");
    setTimeout(function(){
      $("body").removeClass("game-over");
    }, 200);

    //Change title case game-Over
    $("#level-title").text("Game Over, Press Any Key to Restart");

    //StartOver when hit wrong Sequence
    startOver();
  }
}

//StartOver Function
function startOver(){

  //Reset values
  level = 0;
  gamePattern = [];
  started = false;
}
